﻿using IOTLinkAddon.Common;
using IOTLinkAddon.Common.Helpers;
using IOTLinkAddon.Common.Printers;
using IOTLinkAPI.Addons;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.Events;
using IOTLinkAPI.Platform.Windows;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace IOTLinkAddon.Agent
{
    public class PrinterMonitorAgent : AgentAddon
    {
        public override void Init(IAddonManager addonManager)
        {
            base.Init(addonManager);
            OnAgentRequestHandler += OnAgentRequest;
        }

        private void OnAgentRequest(object sender, AgentAddonRequestEventArgs e)
        {
            LoggerHelper.Verbose("PrinterMonitorAgent::OnAgentRequest");

            AddonRequestType requestType = e.Data.requestType;
            switch (requestType)
            {
                case AddonRequestType.REQUEST_PRINTER_INFORMATION:
                    ExecutePrinterInformation(e.Data.printerId);
                    break;

                default: break;
            }
        }

        private void ExecutePrinterInformation(dynamic data)
        {
            try
            {
                string printerId = (string)data;
                LoggerHelper.Verbose("PrinterMonitorAgent::ExecutePrinterInformation({0}) - Started", printerId);

                PrinterInformation printerInfo = PrinterHelper.GetPrinter(printerId);
                if (printerInfo == null)
                {
                    LoggerHelper.Debug("PrinterMonitorAgent::ExecutePrinterInformation({0}) - Printer not found", printerId);
                    return;
                }

                dynamic addonData = new ExpandoObject();
                addonData.requestType = AddonRequestType.REQUEST_PRINTER_INFORMATION;
                addonData.requestData = FillPrinterInformation(printerInfo);

                GetManager().SendAgentResponse(this, addonData);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("PrinterMonitorAgent::ExecutePrinterInformation() - Error: {0}", ex);
            }
        }

        private PrinterInformation FillPrinterInformation(PrinterInformation printerInfo)
        {
            if (printerInfo == null)
                return null;

            LoggerHelper.Debug("PrinterMonitorAgent::FillPrinterInformation({0}) - Started", printerInfo.Id);

            printerInfo.PrinterJobs = PrinterHelper.GetPrinterJobs(printerInfo);
            printerInfo.Queued = printerInfo.Queued || printerInfo.PrinterJobs.Any(job => !job.Completed);

            return printerInfo;
        }
    }
}
