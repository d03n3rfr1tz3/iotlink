﻿using IOTLinkAPI.Configs;
using System.Collections.Generic;

namespace IOTLinkAddon.Common.Configs
{
    public class PrinterConfig
    {
        public bool Discoverable { get; set; }
        public bool Cacheable { get; set; }
        public int Interval { get; set; }
        public bool DefaultPrinter { get; set; }
        public bool LocalPrinters { get; set; }
        public bool NetworkPrinters { get; set; }
        public List<string> Names { get; set; }

        public static PrinterConfig FromConfiguration(Configuration configuration)
        {
            return new PrinterConfig
            {
                Discoverable = configuration.GetValue("discoverable", false),
                Cacheable = configuration.GetValue("cacheable", false),
                Interval = configuration.GetValue("interval", 0),
                DefaultPrinter = configuration.GetValue("defaultPrinter", false),
                LocalPrinters = configuration.GetValue("localPrinters", false),
                NetworkPrinters = configuration.GetValue("networkPrinters", false),
                Names = configuration.GetList<string>("names")
            };
        }
    }
}
