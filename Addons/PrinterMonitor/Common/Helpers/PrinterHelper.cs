﻿using IOTLinkAddon.Common.Configs;
using IOTLinkAddon.Common.Printers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Printing;

namespace IOTLinkAddon.Common.Helpers
{
    public static class PrinterHelper
    {
        public static readonly string DEFAULT_SCOPE = @"\\.\root\CIMV2";
        public static readonly ManagementScope DEFAULT_SCOPE_OBJECT = new ManagementScope(DEFAULT_SCOPE);
        public static readonly ManagementEventWatcher DEFAULT_SCOPE_CREATE_WATCHER = new ManagementEventWatcher
        {
            Scope = DEFAULT_SCOPE_OBJECT,
            Query = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 0.1 WHERE TargetInstance ISA \"Win32_PrintJob\"")
        };
        public static readonly ManagementEventWatcher DEFAULT_SCOPE_MODIFY_WATCHER = new ManagementEventWatcher
        {
            Scope = DEFAULT_SCOPE_OBJECT,
            Query = new WqlEventQuery("SELECT * FROM __InstanceModificationEvent WITHIN 1 WHERE TargetInstance ISA \"Win32_PrintJob\"")
        };
        public static readonly ManagementEventWatcher DEFAULT_SCOPE_DELETE_WATCHER = new ManagementEventWatcher
        {
            Scope = DEFAULT_SCOPE_OBJECT,
            Query = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 1 WHERE TargetInstance ISA \"Win32_PrintJob\"")
        };
        public static readonly List<Action> WatcherActions = new List<Action>();

        public static void AddEvent(Action action)
        {
            if (!WatcherActions.Contains(action)) WatcherActions.Add(action);
        }

        public static void RemoveEvent(Action action)
        {
            if (WatcherActions.Contains(action)) WatcherActions.Remove(action);
        }

        public static void StartWatcher()
        {
            DEFAULT_SCOPE_CREATE_WATCHER.Options.Timeout = TimeSpan.FromSeconds(1);
            DEFAULT_SCOPE_CREATE_WATCHER.EventArrived += DEFAULT_SCOPE_WATCHER_EventArrived;
            DEFAULT_SCOPE_CREATE_WATCHER.Start();

            DEFAULT_SCOPE_MODIFY_WATCHER.Options.Timeout = TimeSpan.FromSeconds(1);
            DEFAULT_SCOPE_MODIFY_WATCHER.EventArrived += DEFAULT_SCOPE_WATCHER_EventArrived;
            DEFAULT_SCOPE_MODIFY_WATCHER.Start();

            DEFAULT_SCOPE_DELETE_WATCHER.Options.Timeout = TimeSpan.FromSeconds(1);
            DEFAULT_SCOPE_DELETE_WATCHER.EventArrived += DEFAULT_SCOPE_WATCHER_EventArrived;
            DEFAULT_SCOPE_DELETE_WATCHER.Start();
        }

        public static void StopWatcher()
        {
            DEFAULT_SCOPE_CREATE_WATCHER.EventArrived -= DEFAULT_SCOPE_WATCHER_EventArrived;
            DEFAULT_SCOPE_CREATE_WATCHER.Stop();

            DEFAULT_SCOPE_MODIFY_WATCHER.EventArrived -= DEFAULT_SCOPE_WATCHER_EventArrived;
            DEFAULT_SCOPE_MODIFY_WATCHER.Stop();

            DEFAULT_SCOPE_DELETE_WATCHER.EventArrived -= DEFAULT_SCOPE_WATCHER_EventArrived;
            DEFAULT_SCOPE_DELETE_WATCHER.Stop();
        }

        public static List<PrinterInformation> GetPrinters(PrinterConfig configuration)
        {
            if (!configuration.DefaultPrinter &&
                !configuration.LocalPrinters &&
                !configuration.NetworkPrinters &&
                !configuration.Names.Any())
                return new List<PrinterInformation>();

            var where = new List<string>();
            if (configuration.DefaultPrinter) where.Add("Default = 1");
            if (configuration.LocalPrinters) where.Add("Local = 1");
            if (configuration.NetworkPrinters) where.Add("Network = 1");
            if (configuration.Names.Any()) where.AddRange(configuration.Names.Select(name => $"Name = '{name}'"));

            var query = string.Format("SELECT * FROM Win32_Printer {0}", where.Any() ? string.Concat("WHERE ", string.Join(" OR ", where)) : string.Empty);
            ManagementObjectSearcher search = new ManagementObjectSearcher(DEFAULT_SCOPE, query);
            ManagementObjectCollection resultCollection = search.Get();

            return resultCollection.Cast<ManagementObject>().Select(x => ParsePrinter(x)).Where(x => x != null).ToList();
        }

        public static PrinterInformation GetPrinter(string printerId)
        {
            if (string.IsNullOrWhiteSpace(printerId))
                return null;

            var query = string.Format("SELECT * FROM Win32_Printer WHERE DeviceID = '{0}'", printerId);
            ManagementObjectSearcher search = new ManagementObjectSearcher(DEFAULT_SCOPE, query);
            ManagementObjectCollection resultCollection = search.Get();

            return resultCollection.Cast<ManagementObject>().Select(x => ParsePrinter(x)).Where(x => x != null).FirstOrDefault();
        }

        public static List<PrinterJobInformation> GetPrinterJobs(PrinterInformation printer)
        {
            PrintServer srv = printer.Local ? new LocalPrintServer() : new PrintServer(printer.ServerName);
            PrintQueue queue = srv.GetPrintQueue(printer.Local ? printer.Name : printer.ShareName);

            return queue.GetPrintJobInfoCollection().Cast<PrintSystemJobInfo>().Select(x => ParsePrinterJob(x)).Where(x => x != null).ToList();
        }

        private static PrinterInformation ParsePrinter(ManagementObject printer)
        {
            if (printer == null) return null;

            PrinterInformation result = new PrinterInformation
            {
                Id = (string)printer.GetPropertyValue("DeviceID"),
                Name = (string)printer.GetPropertyValue("Name"),
                Caption = (string)printer.GetPropertyValue("Caption"),
                Description = (string)printer.GetPropertyValue("Description"),
                Status = (PrinterStatus)(ushort)printer.GetPropertyValue("PrinterStatus"),
                Default = (bool)printer.GetPropertyValue("Default"),
                Direct = (bool)printer.GetPropertyValue("Direct"),
                Local = (bool)printer.GetPropertyValue("Local"),
                Network = (bool)printer.GetPropertyValue("Network"),
                Published = (bool)printer.GetPropertyValue("Published"),
                Queued = (bool)printer.GetPropertyValue("Queued"),
                WorkOffline = (bool)printer.GetPropertyValue("WorkOffline"),
                ErrorStatus = (PrinterErrorStatus)(ushort)printer.GetPropertyValue("DetectedErrorState"),
                ErrorDescription = (string)printer.GetPropertyValue("ErrorDescription"),
                Location = (string)printer.GetPropertyValue("Location"),
                PortName = (string)printer.GetPropertyValue("PortName"),
                ServerName = (string)printer.GetPropertyValue("ServerName"),
                ShareName = (string)printer.GetPropertyValue("ShareName")
            };

            return result;
        }

        private static PrinterJobInformation ParsePrinterJob(PrintSystemJobInfo printJob)
        {
            if (printJob == null) return null;

            PrinterJobInformation result = new PrinterJobInformation
            {
                Id = printJob.JobIdentifier,
                Name = printJob.JobName,
                Caption = printJob.Name,
                Status = printJob.JobStatus,
                TimeSubmitted = new DateTime(printJob.TimeJobSubmitted.Ticks, DateTimeKind.Utc),
                Submitter = printJob.Submitter,
                PagesPrinted = printJob.NumberOfPagesPrinted,
                PagesTotal = printJob.NumberOfPages,
                Priority = (uint)printJob.Priority,
                QueuePosition = printJob.PositionInPrintQueue,
                Blocked = printJob.IsBlocked,
                Completed = printJob.IsCompleted,
                Offline = printJob.IsOffline,
                Error = printJob.IsInError,
                PaperOut = printJob.IsPaperOut,
                Paused = printJob.IsPaused,
                Printed = printJob.IsPrinted,
                Printing = printJob.IsPrinting,
                UserInterventionRequired = printJob.IsUserInterventionRequired
            };

            return result;
        }

        private static void DEFAULT_SCOPE_WATCHER_EventArrived(object sender, EventArrivedEventArgs e)
        {
            foreach (var action in WatcherActions)
            {
                action();
            }
        }
    }
}
