﻿using System.Collections.Generic;

namespace IOTLinkAddon.Common
{
    class PrinterInfoMQTT
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int QueueCount { get; set; }
        public bool Default { get; set; }
        public bool Direct { get; set; }
        public bool Local { get; set; }
        public bool Network { get; set; }
        public bool Published { get; set; }
        public bool Queued { get; set; }
        public bool WorkOffline { get; set; }
        public string ErrorStatus { get; set; }
        public string ErrorDescription { get; set; }
        public string Location { get; set; }
        public string PortName { get; set; }
        public string ServerName { get; set; }
        public string ShareName { get; set; }
    }
}
