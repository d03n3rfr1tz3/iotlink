﻿using System.Collections.Generic;

namespace IOTLinkAddon.Common
{
    class PrinterJobInfoMQTT
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public string Status { get; set; }
        public string TimeSubmitted { get; set; }
        public string Submitter { get; set; }
        public int PagesPrinted { get; set; }
        public int PagesTotal { get; set; }
        public uint Priority { get; set; }
        public int QueuePosition { get; set; }
        public bool Blocked { get; set; }
        public bool Completed { get; set; }
        public bool Offline { get; set; }
        public bool Error { get; set; }
        public bool PaperOut { get; set; }
        public bool Paused { get; set; }
        public bool Printed { get; set; }
        public bool Printing { get; set; }
        public bool UserInterventionRequired { get; set; }
    }
}
