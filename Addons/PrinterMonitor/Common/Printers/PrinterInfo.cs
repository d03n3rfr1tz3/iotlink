﻿using IOTLinkAPI.Platform;

namespace IOTLinkAddon.Common.Printers
{
    public class PrinterInfo
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public PrinterStatus Status { get; set; }

        public bool Default { get; set; } = false;

        public bool Direct { get; set; } = false;

        public bool Local { get; set; } = false;

        public bool Network { get; set; } = false;

        public bool Published { get; set; } = false;

        public bool Queued { get; set; } = false;

        public bool WorkOffline { get; set; } = false;

        public PrinterErrorStatus ErrorStatus { get; set; }

        public string ErrorDescription { get; set; }

        public string Location { get; set; }

        public string PortName { get; set; }

        public string ServerName { get; set; }

        public string ShareName { get; set; }
    }
}
