﻿using IOTLinkAPI.Platform;
using System.Collections.Generic;

namespace IOTLinkAddon.Common.Printers
{
    public class PrinterInformation : PrinterInfo
    {
        public IList<PrinterJobInformation> PrinterJobs { get; set; } = new List<PrinterJobInformation>();
    }
}
