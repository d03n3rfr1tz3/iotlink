﻿using IOTLinkAPI.Platform;
using System;
using System.Printing;

namespace IOTLinkAddon.Common.Printers
{
    public class PrinterJobInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Caption { get; set; }

        public PrintJobStatus Status { get; set; }

        public DateTime TimeSubmitted { get; set; }

        public string Submitter { get; set; }

        public int PagesPrinted { get; set; }

        public int PagesTotal { get; set; }

        public uint Priority { get; set; }

        public int QueuePosition { get; set; }

        public bool Blocked { get; set; } = false;

        public bool Completed { get; set; } = false;

        public bool Offline { get; set; } = false;

        public bool Error { get; set; } = false;

        public bool PaperOut { get; set; } = false;

        public bool Paused { get; set; } = false;

        public bool Printed { get; set; } = false;

        public bool Printing { get; set; } = false;

        public bool UserInterventionRequired { get; set; } = false;
    }
}
