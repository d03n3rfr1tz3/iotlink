﻿using IOTLinkAPI.Platform;

namespace IOTLinkAddon.Common.Printers
{
    public enum PrinterStatus
    {
        Unavailable = 0,
        Other = 1,
        Unknown = 2,
        Idle = 3,
        Printing = 4,
        Warmup = 5,
        StoppedPrinting = 6,
        Offline = 7
    }
}
