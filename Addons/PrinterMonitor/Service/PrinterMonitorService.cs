﻿using IOTLinkAddon.Common;
using IOTLinkAddon.Common.Configs;
using IOTLinkAddon.Common.Helpers;
using IOTLinkAddon.Common.Printers;
using IOTLinkAPI.Addons;
using IOTLinkAPI.Configs;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform;
using IOTLinkAPI.Platform.Events;
using IOTLinkAPI.Platform.HomeAssistant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Timers;

namespace IOTLinkAddon.Service
{
    public class PrinterMonitorService : ServiceAddon
    {
        private static readonly string PAYLOAD_ON = "ON";
        private static readonly string PAYLOAD_OFF = "OFF";

        private string _configPath;
        private Configuration _config;
        private PrinterConfig _printerConfig;

        private Dictionary<string, string> _cache = new Dictionary<string, string>();
        private List<string> _discoveryItems = new List<string>();

        private Timer _cacheTimer;
        private Timer _monitorTimer;

        public override void Init(IAddonManager addonManager)
        {
            base.Init(addonManager);

            LoggerHelper.Verbose("ProcessMonitorService::Init() - Started");

            var cfgManager = ConfigurationManager.GetInstance();
            _configPath = Path.Combine(_currentPath, "config.yaml");
            _config = cfgManager.GetConfiguration(_configPath);
            cfgManager.SetReloadHandler(_configPath, OnConfigReload);

            OnConfigReloadHandler += OnConfigReload;
            OnMQTTConnectedHandler += OnMQTTConnected;
            OnMQTTDisconnectedHandler += OnMQTTDisconnected;
            OnRefreshRequestedHandler += OnClearEvent;
            OnAgentResponseHandler += OnAgentResponse;

            Restart();

            LoggerHelper.Verbose("ProcessMonitorService::Init() - Completed");
        }

        private bool IsAddonEnabled()
        {
            if (_config == null || !_config.GetValue("enabled", false))
            {
                CleanTimers();
                return false;
            }

            return true;
        }

        private void Restart()
        {
            SetupEventMonitor();
            if (!IsAddonEnabled())
                return;

            ClearCaches();
            SetupMonitors();
            SetupDiscovery();
            StartTimers();
        }

        private void SetupEventMonitor()
        {
            if (IsAddonEnabled())
            {
                PrinterHelper.AddEvent(ExecuteMonitors);
                PrinterHelper.StartWatcher();
            }
            else
            {
                PrinterHelper.RemoveEvent(ExecuteMonitors);
                PrinterHelper.StopWatcher();
            }
        }

        private void SetupMonitors()
        {
            _printerConfig = PrinterConfig.FromConfiguration(_config.GetValue("printers"));
        }

        private void SetupDiscovery()
        {
            if (!_printerConfig.Discoverable)
            {
                LoggerHelper.Debug("PrinterMonitorService::SetupDiscovery() - Discovery is disabled.");
                return;
            }

            var printers = PrinterHelper.GetPrinters(_printerConfig);
            foreach (var printer in printers)
            {
                SetupPrinterDiscovery(printer);
            }
        }

        private void SetupPrinterDiscovery(PrinterInformation printer)
        {
            if (_discoveryItems.Contains(printer.Id)) return;
            _discoveryItems.Add(printer.Id);

            var prefixName = $"Printer {printer.Id}";

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/State", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "PowerStatus",
                Name = $"{prefixName} Power Status",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON,
                DeviceClass = "plug"
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/IsDefault", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "IsDefault",
                Name = $"{prefixName} is Default Printer",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/IsDirect", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "IsDirect",
                Name = $"{prefixName} is Direct Printer",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/IsError", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "IsError",
                Name = $"{prefixName} is in Error State",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON,
                DeviceClass = "problem"
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/IsNetwork", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "IsNetwork",
                Name = $"{prefixName} is Network Printer",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON,
                DeviceClass = "connectivity"
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/IsQueued", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "IsQueued",
                Name = $"{prefixName} is a Job Queued",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON,
                DeviceClass = "moving"
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/IsWorkOffline", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.BinarySensor,
                Id = "IsWorkOffline",
                Name = $"{prefixName} is in Work Offline",
                PayloadOff = PAYLOAD_OFF,
                PayloadOn = PAYLOAD_ON
            });

            GetManager().PublishDiscoveryMessage(this, $"{printer.Id}/QueueCount", prefixName, new HassDiscoveryOptions
            {
                Component = HomeAssistantComponent.Sensor,
                Id = "QueueCount",
                Name = $"{prefixName} Queue Count",
                Unit = "jobs",
                Icon = "mdi:printer"
            });
        }

        private void CleanTimers()
        {
            if (_cacheTimer != null)
            {
                _cacheTimer.Stop();
                _cacheTimer.Dispose();
                _cacheTimer = null;
            }

            if (_monitorTimer != null)
            {
                _monitorTimer.Stop();
                _monitorTimer.Dispose();
                _monitorTimer = null;
            }
        }

        private void StartTimers()
        {
            if (!IsAddonEnabled())
                return;

            CleanTimers();

            _cacheTimer = new Timer();
            _cacheTimer.Elapsed += new ElapsedEventHandler(OnCacheTimerElapsed);
            _cacheTimer.Interval = _printerConfig.Interval * 5 * 1000;
            _cacheTimer.Start();

            _monitorTimer = new Timer();
            _monitorTimer.Elapsed += new ElapsedEventHandler(OnMonitorTimerElapsed);
            _monitorTimer.Interval = _printerConfig.Interval * 1000;
            _monitorTimer.Start();
        }

        private void RestartTimers()
        {
            _monitorTimer?.Stop();
            _monitorTimer?.Start();
        }

        private void StopTimers()
        {
            _monitorTimer?.Stop();
        }

        private void OnMQTTConnected(object sender, EventArgs e)
        {
            if (!IsAddonEnabled())
                return;

            ClearCaches();
            SetupMonitors();
            StartTimers();
        }

        private void OnMQTTDisconnected(object sender, EventArgs e)
        {
            StopTimers();
        }

        private void OnClearEvent(object sender, EventArgs e)
        {
            LoggerHelper.Verbose("PrinterMonitorService::OnClearEvent() - Clearing cache and resending information.");

            ClearCaches();
            ExecuteMonitors();
        }

        private void OnConfigReload(object sender, ConfigReloadEventArgs e)
        {
            if (e.ConfigType != ConfigType.CONFIGURATION_ADDON)
                return;

            LoggerHelper.Verbose("PrinterMonitorService::OnConfigReload() - Reloading configuration");

            _config = ConfigurationManager.GetInstance().GetConfiguration(_configPath);
            Restart();
        }
        
        private void OnCacheTimerElapsed(object source, ElapsedEventArgs e)
        {
            LoggerHelper.Verbose("PrinterMonitorService::OnCacheTimerElapsed() - Started");

            ClearCaches(discovery: false);

            LoggerHelper.Verbose("PrinterMonitorService::OnCacheTimerElapsed() - Completed");
        }

        private void OnMonitorTimerElapsed(object source, ElapsedEventArgs e)
        {
            LoggerHelper.Verbose("PrinterMonitorService::OnMonitorTimerElapsed() - Started");

            ExecuteMonitors();

            LoggerHelper.Verbose("PrinterMonitorService::OnMonitorTimerElapsed() - Completed");
        }

        private void ClearCaches(bool discovery = true)
        {
            _cache.Clear();
            if (discovery) _discoveryItems.Clear();
        }

        private void ExecuteMonitors()
        {
            StopTimers();

            LoggerHelper.Verbose("PrinterMonitorService::ExecuteMonitors() - Started");

            var printers = PrinterHelper.GetPrinters(_printerConfig);

            LoggerHelper.Verbose("PrinterMonitorService::ExecuteMonitors() - Found {0} printers", printers.Count);

            ProcessGroup(printers);

            LoggerHelper.Verbose("PrinterMonitorService::ExecuteMonitors() - Completed");

            RestartTimers();
        }

        private void ProcessGroup(List<PrinterInformation> printers)
        {
            LoggerHelper.Verbose("PrinterMonitorService::ProcessGroup() - Started");

            foreach (PrinterInformation printer in printers)
            {
                try
                {
                    SetupPrinterDiscovery(printer);
                    ProcessSingle(printer);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("PrinterMonitorService::ProcessGroup({0}) - Exception: {1}", printer.Name, ex);
                }
            }

            LoggerHelper.Verbose("PrinterMonitorService::ProcessGroup() - Completed");
        }

        private void ProcessSingle(PrinterInformation printer)
        {
            LoggerHelper.Verbose("PrinterMonitorService::ProcessSingle() - Started");

            var printerState = PAYLOAD_ON;
            if (printer.Status == PrinterStatus.Unavailable) printerState = PAYLOAD_OFF;
            if (printer.Status == PrinterStatus.Offline) printerState = PAYLOAD_OFF;
            SendMonitorValue($"{printer.Id}/State", printerState);

            SendMonitorValue($"{printer.Id}/IsDefault", printer.Default ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsDirect", printer.Direct ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsError", printer.ErrorStatus > PrinterErrorStatus.NoError ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsNetwork", printer.Network ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsWorkOffline", printer.WorkOffline ? PAYLOAD_ON : PAYLOAD_OFF);

            RequestAgentData(printer.Id);
            LoggerHelper.Verbose("PrinterMonitorService::ProcessSingle() - Completed");
        }

        private void RequestAgentData(string printerId)
        {
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_PRINTER_INFORMATION;
            addonData.printerId = printerId;

            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnAgentResponse(object sender, AgentAddonResponseEventArgs e)
        {
            AddonRequestType requestType = (AddonRequestType)e.Data.requestType;
            if (requestType != AddonRequestType.REQUEST_PRINTER_INFORMATION)
                return;

            PrinterInformation printer = e.Data.requestData.ToObject<PrinterInformation>();
            OnProcessInformationReceived(printer);
        }

        private void OnProcessInformationReceived(PrinterInformation printer)
        {
            if (printer == null)
                return;

            SendPrinterInformation(printer);
        }

        private void SendPrinterInformation(PrinterInformation printer)
        {
            if (printer == null)
                return;

            LoggerHelper.Debug("PrinterMonitorService::SendProcessInformation() - {1}", JsonConvert.SerializeObject(printer));

            var printerState = PAYLOAD_ON;
            if (printer.Status == PrinterStatus.Unavailable) printerState = PAYLOAD_OFF;
            if (printer.Status == PrinterStatus.Offline) printerState = PAYLOAD_OFF;
            SendMonitorValue($"{printer.Id}/State", printerState);

            SendMonitorValue($"{printer.Id}/IsDefault", printer.Default ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsDirect", printer.Direct ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsError", printer.ErrorStatus > PrinterErrorStatus.NoError ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsNetwork", printer.Network ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsQueued", printer.Queued ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/IsWorkOffline", printer.WorkOffline ? PAYLOAD_ON : PAYLOAD_OFF);
            SendMonitorValue($"{printer.Id}/QueueCount", printer.PrinterJobs.Count.ToString());

            PrinterInfoMQTT mqttPrinter = CreatePrinterInfoMQTT(printer);
            var jsonPrinter = JsonConvert.SerializeObject(mqttPrinter, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            SendMonitorValue($"{printer.Id}/Details", jsonPrinter);

            var mqttJobs = printer.PrinterJobs.Select(printerJob => CreatePrinterJobInfoMQTT(printerJob)).ToList();
            var jsonJobs = JsonConvert.SerializeObject(mqttJobs, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            SendMonitorValue($"{printer.Id}/Jobs", jsonJobs);
        }

        private PrinterInfoMQTT CreatePrinterInfoMQTT(PrinterInformation printer)
        {
            if (printer == null)
                return null;

            PrinterInfoMQTT mqtt = new PrinterInfoMQTT
            {
                Id = printer.Id,
                Name = printer.Name,
                Caption = printer.Caption,
                Description = printer.Description,
                Status = printer.Status.ToString("G"),
                Default = printer.Default,
                Direct = printer.Direct,
                Local = printer.Local,
                Network = printer.Network,
                Published = printer.Published,
                Queued = printer.Queued,
                WorkOffline = printer.WorkOffline,
                ErrorStatus = printer.ErrorStatus.ToString("G"),
                ErrorDescription = printer.ErrorDescription,
                Location = printer.Location,
                PortName = printer.PortName,
                ServerName = printer.ServerName,
                ShareName = printer.ShareName,
                QueueCount = printer.PrinterJobs.Count
            };

            return mqtt;
        }

        private PrinterJobInfoMQTT CreatePrinterJobInfoMQTT(PrinterJobInformation printerJob)
        {
            if (printerJob == null)
                return null;

            var mqtt = new PrinterJobInfoMQTT
            {
                Id = printerJob.Id,
                Name = printerJob.Name,
                Caption = printerJob.Caption,
                Status = printerJob.Status.ToString("G"),
                TimeSubmitted = FormatDateObject(printerJob.TimeSubmitted),
                Submitter = printerJob.Submitter,
                PagesPrinted = printerJob.PagesPrinted,
                PagesTotal = printerJob.PagesTotal,
                Priority = printerJob.Priority,
                QueuePosition = printerJob.QueuePosition,
                Blocked = printerJob.Blocked,
                Completed = printerJob.Completed,
                Offline = printerJob.Offline,
                Error = printerJob.Error,
                PaperOut = printerJob.PaperOut,
                Paused = printerJob.Paused,
                Printed = printerJob.Printed,
                Printing = printerJob.Printing,
                UserInterventionRequired = printerJob.UserInterventionRequired
            };

            return mqtt;
        }

        private string FormatDateObject(object value)
        {
            var format = _config.GetValue("formats:dateTimeFormat", "yyyy-MM-dd HH:mm:ss");
            if (value is DateTime)
                return ((DateTime)value).ToString(format, CultureInfo.InvariantCulture);

            return value.ToString();
        }

        private void SendMonitorValue(string topic, string value)
        {
            if (string.IsNullOrWhiteSpace(topic))
                return;

            if (_printerConfig.Cacheable)
            {
                if (_cache.ContainsKey(topic) && _cache[topic].CompareTo(value) == 0)
                    return;

                _cache[topic] = value;
            }

            GetManager().PublishMessage(this, topic, value);
        }
    }
}